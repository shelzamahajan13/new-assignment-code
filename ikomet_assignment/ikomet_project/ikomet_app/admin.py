from django.contrib import admin
from django.contrib import admin

from .models import QuoteStatus,PartNotable,QuoteDetails,PartDetails




class QuoteStatusAdmin(admin.ModelAdmin):
	
	list_display = ('id', 'quote_name','quote_keyword')
	search_fields = ('id', 'quote_name','quote_keyword')

class PartNotableAdmin(admin.ModelAdmin):
	
	list_display = ('id', 'part_name','part_keyword')
	search_fields =  ('id', 'part_name','part_keyword')

class QuoteDetailsAdmin(admin.ModelAdmin):
	
	list_display = ('id', 'quote_status_id','quote_number','quote_date','person_name','sale_person','part_total','gst_total','total_amount')
	search_fields = ('id', 'quote_status_id','quote_number','quote_date','person_name','sale_person','part_total','gst_total','total_amount')
	row_id_fields=('quote_status',)
	
	def quote_status(self, obj):
		return obj.quote_status_id.id

class PartDetailsAdmin(admin.ModelAdmin):
	
	list_display = ('id', 'quote_details_id','part_id','description','qty','cost','gst','total')
	search_fields =('id', 'quote_details_id','part_id','description','qty','cost','gst','total')
	row_id_fields=('quote_details_id','part_id')
	
	def quote_details(self, obj):
		return obj.quote_details_id.id	
	def part_details(self,obj):
		return obj.part_id.id


	

admin.site.register(QuoteStatus,QuoteStatusAdmin)
admin.site.register(PartNotable,PartNotableAdmin)
admin.site.register(QuoteDetails,QuoteDetailsAdmin)
admin.site.register(PartDetails,PartDetailsAdmin)
