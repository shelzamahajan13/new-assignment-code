

app.controller("quote_controller",function($scope,$http,CONFIG){

	

	$scope.validation=function(type,value){
		if(type == "quote_number"  && value!== undefined){
			
			if(value!=''){
				if(type == "quote_number"){
					$scope.quote_number_err= ""; 
					return true;
				}
			}else{
				
				$scope.quote_number_err = "Enter quote number";
				return false;
			} 
		}else if((type == "quote_number") && value == undefined){
				$scope.quote_number_err = "Quote number required";
				return false;
		}

		if(type == "customer_name"  && value!== undefined){
			
			if(value!=''){
				if(type == "customer_name"){
					$scope.customer_name_err= ""; 
					return true;
				}
			}else{
				
				$scope.customer_name_err = "Enter customer name";
				return false;
			} 
		}else if((type == "customer_name") && value == undefined){
				$scope.customer_name_err = "Customer name required";
				return false;
		
		}

		if(type == "sales_person"  && value!== undefined){
			
			if(value!=''){
				if(type == "sales_person"){
					$scope.sales_person_err= ""; 
					return true;
				}
			}else{
				
				$scope.sales_person_err = "Enter sales person";
				return false;
			} 
		}else if((type == "sales_person") && value == undefined){
				$scope.sales_person_err = "Sales person required";
				return false;
		
		}

		if(type == "quote_status"  && value!== undefined){
		
			if(value!='0'){
				if(type == "quote_status"){
					$scope.quote_status_err= ""; 
					return true;
				}
			}else{
				
				$scope.quote_status_err = "Select quote status";
				return false;
			} 
		}else if((type == "quote_status") && value == undefined){
				$scope.quote_status_err = "Quote status required";
				return false;
		
		}

		if(type == "part_no"  && value!== undefined){
			console.log(value)
			if(value!='0'){
				if(type == "part_no"){
					$scope.get_part_value_err= ""; 
					return true;
				}
			}else{
				
				$scope.get_part_value_err = "Select part";
				return false;
			} 
		}else if((type == "part_no") && value == undefined){
				$scope.get_part_value_err = "Part required";
				return false;
		
		}



		
	}

	// $(".table_structure").hide()
	$scope.table_values = [];
	counter=0
	var arr=[]

	index=0

	$scope.duplicate_array = [];
	
	$scope.add_dynamic_lines=function(user){
		
		var data_set= {
	        part_no: '',
	        description: '',
	        qty:'',
	        cost:'',
	        gst:'',
	        total:'',
	        gst_calculate:'',
	        part_result_set:''
	        
	   	};

	   	
 		$(".table_structure").show()
 		
 		arr=$scope.table_values
 		counter++
 		
 		
 		if(counter!='1'){
 			if(arr.length!='0'){
	 			
	 			if($scope.table_values[arr.length-1].qty!='' && $scope.table_values[arr.length-1].cost!=='' && $scope.table_values[arr.length-1].part_no!=='0' && $scope.table_values[arr.length-1].description!=='' && $scope.table_values[arr.length-1].total!=='' && $scope.table_values[arr.length-1].gst!==''){
	 				$scope.table_values.push(data_set);

			 		$scope.duplicate_array.push({'qty':$scope.table_values[index].qty,'cost':$scope.table_values[index].cost,'gst_calculate':$scope.table_values[index].gst_calculate})
			 		console.log($scope.duplicate_array)

			 		index++


	 			}else{
	 				alert("Please fill the line details")
	 			}
	 		}


 		}else{
 			

 			$scope.table_values.push(data_set);
 		}


 		
	 	
	}

	$scope.removeRow= function (i) {
       $scope.table_values.splice(i, 1);
      
       var total_part_result_new='0'
       var total_gst_result_new='0'

       if($scope.table_values.length=='0'){
       		$scope.user.total_part='0'
       		$scope.user.total_gst_calculate='0'
       		$scope.user.total_tax_calculate='0'
       		counter=0
       }else{
       		angular.forEach($scope.table_values,function(value,key){
       			
       			
       		var part_result1=($scope.table_values[key].qty*$scope.table_values[key].cost)

   			total_part_result_new=(parseFloat(total_part_result_new)+parseFloat(part_result1)).toFixed(2)

			var gst_calculate1=($scope.table_values[key].gst_calculate)

			total_gst_result_new=(parseFloat(total_gst_result_new)+parseFloat(gst_calculate1)).toFixed(2)
   			
   			$scope.user.total_part=(parseFloat(total_part_result_new)).toFixed(2)

   			$scope.user.total_gst_calculate=(parseFloat(total_gst_result_new)).toFixed(2)

   			$scope.user.total_tax_calculate=(parseFloat($scope.user.total_part)+parseFloat($scope.user.total_gst_calculate)).toFixed(2)

       		})
       }
   	};

   	
   
   	$scope.calculate_total_cost=function(user){
		$scope.user.total_part='0'
		$scope.user.total_gst_calculate='0'
		$scope.user.total_tax_calculate='0'
		var total_part_result='0'
		var total_gst_result='0'
		
   		var total=(user.qty*user.cost)+((user.gst*user.qty*user.cost)/100)

   		user.total=parseFloat(total).toFixed(2)

   		if($scope.duplicate_array.length=='0'){
   			
   			var part_result=(user.qty*user.cost)
   			user.gst_calculate=((user.gst*part_result)/100)
	
			$scope.user.total_part=parseFloat(part_result).toFixed(2)

			$scope.user.total_gst_calculate=parseFloat(user.gst_calculate).toFixed(2)

			$scope.user.total_tax_calculate=(parseFloat($scope.user.total_part)+parseFloat($scope.user.total_gst_calculate)).toFixed(2)
   		

   		}else{

   			angular.forEach($scope.duplicate_array, function(value,key){

   				if((user.cost=='' && user.qty!='') || (user.cost!='' && user.qty=='')){

   					$scope.user.total_part=$("#total_part").val()
   					$scope.user.total_gst_calculate=$("#gst_cal").val()
   					$scope.user.total_tax_calculate=$("#total_cal").val()

   				}else {
   					var part_result=(user.qty*user.cost)
   					
   					if(user.gst=='0'){
   						
   						user.gst_calculate=parseFloat('0.0')
   					}else{
   						user.gst_calculate=((user.gst*part_result)/100)
   					}
	   		

		   			var part_result1=($scope.duplicate_array[key].qty*$scope.duplicate_array[key].cost)

		   			total_part_result=(parseFloat(total_part_result)+parseFloat(part_result1)).toFixed(2)

	   				var gst_calculate1=($scope.duplicate_array[key].gst_calculate)

	   				total_gst_result=(parseFloat(total_gst_result)+parseFloat(gst_calculate1)).toFixed(2)
		   			
		   			$scope.user.total_part=(parseFloat(total_part_result)+parseFloat(part_result)).toFixed(2)

		   			$scope.user.total_gst_calculate=(parseFloat(total_gst_result)+parseFloat(user.gst_calculate)).toFixed(2)

		   			$scope.user.total_tax_calculate=(parseFloat($scope.user.total_part)+parseFloat($scope.user.total_gst_calculate)).toFixed(2)

   				}
   			
	   			
	   				
	   		})
  

   		}
   		
   		
   	}
   	

   	$scope.submit_part_data_Details=function(user){
   		
   		if($scope.table_values.length=='0'){
   			alert("Please add lines")
   		}else{
   			var quote_number=$scope.validation('quote_number',$scope.user.quote_number)
   			var customer_name=$scope.validation('customer_name',$scope.user.customer_name)
   			var sales_person=$scope.validation('sales_person',$scope.user.sales_person)
   			var quote_status=$scope.validation('quote_status',$scope.user.quote_status)
   			
   			var data=angular.copy(user)
			var data=JSON.stringify(data)
			
			table_values=JSON.stringify($scope.table_values)

			if($scope.table_values[$scope.table_values.length-1].part_no=='0'){
				alert("Select Part no")
			}

			
   			if(quote_number==true && customer_name==true && sales_person==true && quote_status==true){
   				$.ajax({
				url:'/saveQuote/',
				type:'POST',
				dataType: 'json',
				data:{'data':data,"table_values":table_values},
				success: function(response){
					
					var a=response['a_res'];
					

					if(a=='0'){
						 $.confirm({
				        	
				            theme: 'modern',
				            columnClass: 'medium',
				            title: 'Congratulations!!',
				            content: 'Thanks for creating the Quotes and your Quote id is : '+$scope.user.quote_number+'',
				            type: 'green',
				            animation: 'RotateYR',
				            closeAnimation: 'RotateYR',
				            typeAnimated: true,
				            buttons: {
				              ok: function () {
				                location.reload()
				              },
				              
				            }
				        });
					}

					if(a=='1'){
						$.confirm({
				        	
				            theme: 'modern',
				            columnClass: 'medium',
				            title: 'Congratulations!!',
				            content: 'Your information already exist! Some error occur in updating!!',
				            type: 'green',
				            animation: 'RotateYR',
				            closeAnimation: 'RotateYR',
				            typeAnimated: true,
				            buttons: {
				              ok: function () {
				                location.reload()
				              },
				              
				            }
				        });
					}

					
					
				},function (response) {
					if(response.status=="500"){
						console.log('wrong1'); 
					}
				}
			})
   			}

   			
   		}
   	}

   	var BASE_URL=JSON.stringify(CONFIG['BASE_URL']).replace(/\"/g, "");
   	$scope.get_data_quote_number=function(quote_number,user){
   		
   		var data=JSON.stringify({"quote_number":quote_number})
   		$http.post(BASE_URL+"get_data_quote_number/"+data).then(function(response){
   			var data=JSON.stringify(response.data.a_res).replace(/\"/g, "");
		    if(data=='0'){
		    	
				var quote_date=JSON.stringify(response.data.get_quote_details[0]['quote_date']).replace(/\"/g, "")
				if(quote_date!='null' && quote_date!=undefined && quote_date!='' && quote_date!='NULL'){
					$scope.user.quote_date=quote_date
				}else{
					$scope.user.quote_date=''
				}

				var person_name=JSON.stringify(response.data.get_quote_details[0]['person_name']).replace(/\"/g, "")
				if(person_name!='null' && person_name!=undefined && person_name!='' && person_name!='NULL'){
					$scope.user.customer_name=person_name
				}else{
					$scope.user.customer_name=''
				}

				var sale_person=JSON.stringify(response.data.get_quote_details[0]['sale_person']).replace(/\"/g, "")
				if(sale_person!='null' && sale_person!=undefined && sale_person!='' && sale_person!='NULL'){
					$scope.user.sales_person=sale_person
				}else{
					$scope.user.sales_person=''
				}

				var quote_status=JSON.stringify(response.data.get_quote_details[0]['quote_status_id__quote_keyword']).replace(/\"/g, "")
				if(quote_status!='null' && quote_status!=undefined && quote_status!='' && quote_status!='NULL'){
					$scope.user.quote_status=quote_status
				}else{
					$scope.user.quote_status=''
				}


				var part_total=JSON.stringify(response.data.get_quote_details[0]['part_total']).replace(/\"/g, "")
				if(part_total!='null' && part_total!=undefined && part_total!='' && part_total!='NULL'){
					$scope.user.total_part=part_total
				}else{
					$scope.user.total_part=''
				}


				var gst_total=JSON.stringify(response.data.get_quote_details[0]['gst_total']).replace(/\"/g, "")
				if(gst_total!='null' && gst_total!=undefined && gst_total!='' && gst_total!='NULL'){
					$scope.user.total_gst_calculate=gst_total
				}else{
					$scope.user.total_gst_calculate=''
				}


				var total_amount=JSON.stringify(response.data.get_quote_details[0]['total_amount']).replace(/\"/g, "")
				if(total_amount!='null' && total_amount!=undefined && total_amount!='' && total_amount!='NULL'){
					$scope.user.total_tax_calculate=total_amount
				}else{
					$scope.user.total_tax_calculate=''
				}

				table_values=response.data.get_part_details_id
		    	
				console.log(table_values)
				
			
				for(i=0;i<table_values.length;i++){
				
					user.part_no=table_values[i]['part_id__id']
					user.description=table_values[i]['description']
					user.qty=table_values[i]['qty']
					user.gst=table_values[i]['gst']
					user.total=table_values[i]['total']
					user.cost=table_values[i]['cost']
					
					$scope.table_values.push({part_no:user.part_no,

					description:user.description,qty:user.qty,cost:user.cost,gst:user.gst,total:user.total})
					
					console.log($scope.table_values)

				}
			}
   		},function(response){
   			console.log("error")
   		})

   	}


});















   	



  



});