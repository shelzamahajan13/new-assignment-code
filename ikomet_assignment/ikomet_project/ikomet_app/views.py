from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.views.generic import TemplateView,ListView
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

import logging
import requests
import json
import string,re

import io
import sys
from django.utils import timezone
from datetime import datetime,timedelta
import datetime

from.models import QuoteStatus,PartNotable,QuoteDetails,PartDetails



stdlogger = logging.getLogger(__name__)

log = logging.getLogger(__name__)

if __name__ == '__main__':
	sys.exit(main())

# Create your views here.
def IndexPageOnload(request):
	get_server_current_time= timezone.now()
				
	get_current_timestamp= get_server_current_time +datetime.timedelta(hours=5,minutes=30)
	
	
	get_format_current_date=str(get_current_timestamp)[:19]
	
	get_current_timestamp_new = datetime.datetime.strptime(get_format_current_date, '%Y-%m-%d %H:%M:%S')
	get_current_date=get_current_timestamp_new.date()

	get_month=get_current_date.strftime('%b') 
	day=get_current_date.day
	day_double=str(day).zfill(2)
	year=get_current_date.year
	get_current_date_value=str(day_double)+'/'+str(get_month)+'/'+str(year)

	get_current_time=get_current_timestamp_new.time()

	get_current_time_value=get_current_time.strftime("%I:%M:%S %p")
	quote_status=QuoteStatus.objects.all()

	get_part_data=PartNotable.objects.all()
	return render(request,'index.html',{"get_part_data":get_part_data,"quote_status":quote_status,"get_current_date_value":get_current_date_value,"get_current_time_value":get_current_time_value})


@csrf_exempt
def saveQuoteData(request):
	result_set={}
	try:
		req_data=request.POST.get('data')
		
		req_data=json.loads(req_data)

		table_data=request.POST.get('table_values')
		log.info(table_data)

		table_data_str=json.loads(table_data)
		log.info(table_data_str)

	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)


	try:
		if request.method=='POST':
			if not QuoteDetails.objects.filter(quote_number=req_data['quote_number']).exists():
				get_quote_status_instance=QuoteStatus.objects.get(quote_keyword=req_data['quote_status'])
				QuoteDetails.objects.create(quote_status_id=get_quote_status_instance,
					quote_number=req_data['quote_number'],
					quote_date=req_data['quote_date'],
					person_name=req_data['customer_name'],
					sale_person=req_data['sales_person'],
					part_total=req_data['total_part'],
					gst_total=req_data['total_gst_calculate'],
					total_amount=req_data['total_tax_calculate']

					).save()

				get_quote_details_instance=QuoteDetails.objects.get(quote_number=req_data['quote_number'])
				
				for x in table_data_str:
					
					PartDetails.objects.create(
						quote_details_id=get_quote_details_instance,
						part_id=PartNotable.objects.get(id=x['part_no']),
						description=x['description'],
						qty=x['qty'],
						cost=x['cost'],
						gst=x['gst'],
						total=x['total']

						).save()

					
				result_set['a_res']='0'
			else:
				result_set['a_res']='1'
				log.info("exists")

		return HttpResponse(json.dumps(result_set), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)

@csrf_exempt
def get_data_quote_number(request,data):
	result={}
	try:
		req_data=json.loads(data)
		
	except Exception as ex:
			log.error('Error in json data: %s' % ex)
	try:
		if request.method=='POST':
			if QuoteDetails.objects.filter(quote_number=req_data['quote_number']).exists():
				get_quote_details=QuoteDetails.objects.filter(quote_number=req_data['quote_number']).prefetch_related('quote_status_id').values('quote_status_id__quote_keyword','quote_date','person_name','sale_person','part_total','gst_total','total_amount')

				get_quote_id=QuoteDetails.objects.get(quote_number=req_data['quote_number']).id

				get_part_details_id=PartDetails.objects.filter(quote_details_id=get_quote_id).prefetch_related('part_id').values('part_id__id','description','qty','cost','gst','total')
				
				result['get_part_details_id']=list(get_part_details_id)
				result['get_quote_details']=list(get_quote_details)
				result['a_res']='0'
			else:
				result['a_res']='1'
				
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)