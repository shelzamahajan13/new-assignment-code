from django.apps import AppConfig


class IkometAppConfig(AppConfig):
    name = 'ikomet_app'
