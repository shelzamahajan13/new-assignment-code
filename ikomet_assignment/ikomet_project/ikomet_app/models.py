from django.db import models
from django.utils import timezone
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class QuoteStatus(models.Model):
	quote_name=models.CharField(max_length=50,null=True,blank=True)
	quote_keyword=models.CharField(max_length=50,null=True,blank=True)

class PartNotable(models.Model):
	part_name=models.CharField(max_length=50,null=True,blank=True)
	part_keyword=models.CharField(max_length=50,null=True,blank=True)

class QuoteDetails(models.Model):
	quote_status_id=models.ForeignKey(QuoteStatus, null=True,on_delete=models.CASCADE)
	quote_number=models.CharField(max_length=50,null=True,blank=True)
	quote_date=models.CharField(max_length=100,null=True,blank=True)
	person_name=models.CharField(max_length=100,null=True,blank=True)
	sale_person=models.CharField(max_length=100,null=True,blank=True)
	part_total=models.CharField(max_length=100,null=True,blank=True)
	gst_total=models.CharField(max_length=100,null=True,blank=True)
	total_amount=models.CharField(max_length=100,null=True,blank=True)


class PartDetails(models.Model):
	quote_details_id=models.ForeignKey(QuoteDetails, null=True,on_delete=models.CASCADE)
	part_id=models.ForeignKey(PartNotable, null=True,on_delete=models.CASCADE)
	description=models.CharField(max_length=100,null=True,blank=True)
	qty=models.CharField(max_length=50,null=True,blank=True)
	cost=models.CharField(max_length=50,null=True,blank=True)
	gst=models.CharField(max_length=50,null=True,blank=True)
	total=models.CharField(max_length=100,null=True,blank=True)
	

